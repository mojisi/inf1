from math import sin, cos, pi, radians
from random import randint
def new_coords(k_coords=0, length=randint(10,100), x0=randint(-100,100), y0=randint(-100,100)):
    # k_coords - кол-во координат
    # length - расстояние координаты от центра
    # x0, y0 - поциция центра фигуры
    CoordList=[]
    angle = radians(randint(0,360))
    for i in range(k_coords):
        alpha = (2*pi*i)/k_coords + angle
        x = length * cos(alpha) + x0
        y = length * sin(alpha) + y0
        coordsXY = [int(x),int(y)]
        CoordList.append(coordsXY)
    return CoordList

def main(arg):
    coords = new_coords(10,10)
    for i in coords:
        print(f'x={i[0]:<5} y={i[1]}')

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))