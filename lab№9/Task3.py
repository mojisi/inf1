from Task2 import new_coords 

def file_n(array, path):
	with open(path, 'w') as file:
		for i in array:
			file.write(str(i[0]) + '; ' + str(i[1]) + ';\n')
	print('Запись в файл выполнена успешно')

def main(args):
    file = 'data.csv'
    Coords = new_coords(10)
    file_n(Coords, file)

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
