from Task4 import input_file
er_rate=10**(-10)

def figure_middle(x,y):
    while not (calculate(x) and calculate(y)):
        x=middle(x)
        y=middle(y)
    x=sum(x)/len(x)
    y=sum(y)/len(y)
    print('Координаты середины фигуры: ')
    print(f'x = {x:>10} \u2248 {x:+f}')
    print(f'y = {y:>10} \u2248 {y:+f}')

def middle(a):
    return[((a[i]+a[(i+1)%len(a)])/2) for i in range(len(a))]

def calculate(a):
    for i in range(1, len(a)):
        if abs(a[i-1]-a[i%len(a)]) > er_rate:
            return False
        return True

def main(args):
    file = 'data.csv'
    coords = input_file(file)
    x = [i[0] for i in coords]
    y = [i[1] for i in coords]
    figure_middle(x, y)
    input()

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))

